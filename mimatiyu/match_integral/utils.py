from datetime import datetime


def convert_date_str_to_datetime(datestr: str):
    dt = datetime.strptime(datestr, '%Y-%m-%d')
    year = dt.year
    month = dt.month
    day = dt.day
    return year, month, day
