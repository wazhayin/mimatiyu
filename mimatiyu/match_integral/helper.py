import requests
import time


def crawler_match_info():
    headers = {
        "Host": "zq.win007.com",
        "Referer": "http://zq.win007.com/info/index_cn.htm",
        "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36"
    }
    now = time.time()
    hour = time.strftime('%Y%m%d%H', time.localtime(now))
    # 英超:36, 意甲:34, 西甲:34, 德甲:8, 法甲:11
    league_ids = [36, 34, 31, 8, 11]
    # for id in league_ids:
    s = requests.Session()
    for id in league_ids:
        r = s.get('http://zq.win007.com/jsData/matchResult/2018-2019/s{}.js?version={}'.format(id, hour),
                  headers=headers)
        with open('./{}.txt'.format(id), 'w') as f:
            f.write(r.text)


def get_match_info(id):
    with open('match_integral/{}.txt'.format(id), 'r') as f:
        data = f.read()
    return data

