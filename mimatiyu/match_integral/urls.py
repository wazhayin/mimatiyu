from django.urls import path

from match_integral import views

urlpatterns = [
    path('info/', views.Match_Info.as_view()),
    path('banner/', views.Match_Banner.as_view()),
    path('ad/', views.Match_AD.as_view()),
    path('live/', views.Url_Href.as_view()),
    path('recommend/', views.Recommend.as_view()),
]
