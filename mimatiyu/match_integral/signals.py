from django.db.models.signals import post_save
from django.dispatch import receiver
import os
from match_integral.models import UpdateScore


@receiver(post_save, sender=UpdateScore)
def room_save_handler(sender, instance, **kwargs):
    if kwargs['created']:
        os.system('/root/.virtualenvs/mimatiyu/bin/python /root/wang/mimatiyu/mimatiyu/manage.py insert_data')
        print('inserting data...')
    else:
        pass
