import json

from django.http import HttpResponse, FileResponse, JsonResponse
from django.shortcuts import render
import os
# Create your views here.
from django.views.generic.base import View

from match_integral.helper import get_match_info
from match_integral.models import LeagueScore, BannerImage, AdImage, UrlModel, Article
from match_integral.utils import convert_date_str_to_datetime
from mimatiyu.settings import BASE_DIR, MEDIA_URL


class Match_Info(View):

    def get(self, request):
        # 英超:36, 意甲:34, 西甲:31, 德甲:8, 法甲:11
        league_ids = {'法甲': '法国甲组联赛', '西甲': '西班牙甲组联赛', '意甲': '意大利甲组联赛', '英超': '英格兰超级联赛', '德甲': '德国甲组联赛'}
        scores = LeagueScore.objects.all()[:98]
        result = {}
        lists = []
        for score in scores[::-1]:
            scorelist = {
                'key': score.id,
                'league_name':score.league_name,
                'rank': score.ranking,
                'club': score.club_name,
                'win': score.win_count,
                'fair': score.draw_count,
                'lose': score.lose_count,
                'points': score.integral,

            }
            lists.append(scorelist)
        for key in league_ids.keys():
            result[key] = []
            for li in lists:
                if li['league_name'] == league_ids[key]:
                    result[key].append(li)

        return JsonResponse({'data':  [{key: value} for key, value in result.items()]}, safe=False)


class Match_Banner(View):
    def get(self, request):
        images = BannerImage.objects.all()
        result = [image.to_dict() for image in images]
        return JsonResponse(result, safe=False)


class Match_AD(View):
    def get(self, request):
        images = AdImage.objects.all()
        result = [image.to_dict() for image in images]
        return JsonResponse(result, safe=False)


class Url_Href(View):
    def get(self, request):
        url = UrlModel.objects.last()
        result = {
            'href': url.url
        }
        return JsonResponse(result, safe=False)


class Recommend(View):
    def get(self, request):
        datestr = request.GET.get('date')
        if len(datestr) != 8:
            return JsonResponse({'data': []}, safe=False)
        year = datestr[:4]
        month = datestr[4:6]
        day = datestr[6:]
        datadict = {}
        leaguescores = Article.objects.filter(start_time__year=year, start_time__month=month, start_time__day=day).all()
        results = [leaguescore.to_dict() for leaguescore in leaguescores]
        leagues = list(set([result['league'] for result in results]))
        for league in leagues:
            datadict[league] = []
            for result in results:
                if result['league'] == league:
                    datadict[league].append(result)

        return JsonResponse({'data': datadict}, safe=False)
