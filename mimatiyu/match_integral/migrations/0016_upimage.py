# Generated by Django 2.0.3 on 2019-03-28 10:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('match_integral', '0015_auto_20190328_0919'),
    ]

    operations = [
        migrations.CreateModel(
            name='UpImage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('banner_image', models.ImageField(blank=True, null=True, upload_to='')),
                ('ad_image', models.ImageField(blank=True, null=True, upload_to='')),
                ('url', models.CharField(blank=True, max_length=256, null=True, verbose_name='直播链接')),
            ],
            options={
                'db_table': 'image',
            },
        ),
    ]
