import xadmin
from .models import Article, UpdateScore, BannerImage, AdImage, UrlModel

import xadmin
from xadmin import views
from . import models


class BaseSetting(object):
    """xadmin的基本配置"""
    enable_themes = True  # 开启主题切换功能
    use_bootswath = True  # 这一行也得添加上才行


class GlobalSettings(object):
    """xadmin的全局配置"""
    site_title = "密码体育管理系统"  # 设置站点标题
    site_footer = "密码体育"  # 设置站点的页脚
    # menu_style = "accordion"  # 设置菜单折叠


class ArticleAdmin(object):
    list_display = ['create_time', 'start_time', 'league_name', 'home_team',
                    'guest_team', 'guest_odd', 'result', 'recommend']
    search_fields = ['id', 'league_name', ]
    # list_editable = ['id','create_time' ,'start_time','league_name','home_team',
    #                 'guest_team', 'guest_odd', 'result', 'recommend']
    list_filter = ['id', 'create_time', 'start_time', 'league_name', 'home_team',
                   'guest_team', 'guest_odd', 'result']


class BannerImageAdmin(object):
    list_display = ['id', 'create_time', 'banner_image']
    search_fields = ['id', 'create_time', 'banner_image']
    # list_editable = ['id', 'create_time' ,'banner_image']
    list_filter = ['id', 'create_time', 'banner_image']


class AdImageAdmin(object):
    list_display = ['id', 'create_time', 'ad_image']
    search_fields = ['id', 'create_time', 'ad_image']
    # list_editable = ['id', 'create_time' ,'ad_image']
    list_filter = ['id', 'create_time', 'ad_image']


class UrlModelAdmin(object):
    list_display = ['id', 'create_time', 'url']
    search_fields = ['id', 'create_time', 'url']
    # list_editable = ['id', 'create_time' ,'url']
    list_filter = ['id', 'create_time', 'url']


class UpdateScoreAdmin(object):
    list_display = ['id', 'create_time', 'update_info']


xadmin.site.register(views.BaseAdminView, BaseSetting)
xadmin.site.register(views.CommAdminView, GlobalSettings)
xadmin.site.register(Article, ArticleAdmin)
xadmin.site.register(BannerImage, BannerImageAdmin)
xadmin.site.register(AdImage, AdImageAdmin)
xadmin.site.register(UrlModel, UrlModelAdmin)
xadmin.site.register(UpdateScore, UpdateScoreAdmin)