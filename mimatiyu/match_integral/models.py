from django.db import models

# Create your models here.
from mimatiyu.settings import MEDIA_URL


class Article(models.Model):
    RESULT_WIN = 1
    RESULT_LOSE = 2
    RESULT_CHOICES = (
        (RESULT_WIN, '赢'),
        (RESULT_LOSE, '输'),
    )

    HOME_WIN = 1
    GUEST_WIN = 2
    DRAW = 3
    LESS_THAN_2_5 = 4
    MORE_THAN_2_5 = 5
    BOTH_GET_GOAL = 6
    NEITHER_GET_GOAL = 7
    RECOMMEND_CHOICES = (
        (HOME_WIN, '主队赢'),
        (GUEST_WIN, '客队赢'),
        (DRAW, '平局'),
        (LESS_THAN_2_5, '低于2.5球'),
        (MORE_THAN_2_5, '高于2.5球'),
        (BOTH_GET_GOAL, '两个队都进球'),
        (NEITHER_GET_GOAL, '两个队都不进球'),
    )

    id = models.BigAutoField(primary_key=True)
    is_deleted = models.BooleanField(default=False, db_index=True)
    create_time = models.DateTimeField('创建时间', auto_now_add=True)
    start_time = models.DateTimeField('比赛开始时间')
    league_name = models.CharField('联赛名', max_length=256)
    home_team = models.CharField('主队名', max_length=256)
    guest_team = models.CharField('客队名', max_length=256)
    recommend = models.PositiveSmallIntegerField('推荐', choices=RECOMMEND_CHOICES, null=True, blank=True)
    guest_odd = models.DecimalField('赔率', max_digits=16, decimal_places=2, default=0)
    result = models.PositiveSmallIntegerField('结果', choices=RESULT_CHOICES, null=True, blank=True)

    class Meta:
        ordering = ['league_name', '-start_time']
        verbose_name = '比赛'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.league_name + '-' * 3 + self.home_team + ' VS ' + self.guest_team + '-' * 3 + str(self.start_time)

    def to_dict(self):
        result = {
            'time': self.start_time,
            'league': self.league_name,
            'vs': self.home_team + 'VS' + self.guest_team,
            'recommend': self.get_recommend_display(),
            'odd': self.guest_odd,
            'result': self.result,
        }
        return result


class LeagueScore(models.Model):
    id = models.BigAutoField(primary_key=True)
    create_time = models.DateTimeField('创建时间', auto_now_add=True)
    league_name = models.CharField('联赛名', max_length=256)
    ranking = models.IntegerField('排名')
    club_name = models.CharField('俱乐部名', max_length=256)
    win_count = models.IntegerField('赢次数')
    draw_count = models.IntegerField('打平次数')
    lose_count = models.IntegerField('输次数')
    integral = models.IntegerField('积分')

    class Meta:
        ordering = ['-id']


class UpdateScore(models.Model):
    id = models.BigAutoField(primary_key=True)
    create_time = models.DateTimeField('创建时间', auto_now_add=True)
    update_info = models.CharField('备注信息', max_length=256)

    class Meta:
        ordering = ['-id']
        verbose_name = '手动更新积分榜记录'
        verbose_name_plural = verbose_name

    def __str__(self):
        return str(self.id) + '-' * 3 + str(self.create_time) + '-' * 3 + str(self.update_info)


class BannerImage(models.Model):
    id = models.BigAutoField(primary_key=True)
    create_time = models.DateTimeField('创建时间', auto_now_add=True)
    banner_image = models.ImageField('轮播图', upload_to='banner', null=True, blank=True)
    url = models.CharField('链接', max_length=256, null=True, blank=True)

    class Meta:
        ordering = ['-id']
        verbose_name = '轮播图'
        verbose_name_plural = verbose_name

    def to_dict(self):
        result = {
            'id': self.id,
            'create_time': self.create_time,
            'banner_image': MEDIA_URL + str(self.banner_image),
            'href': self.url
        }
        return result


class AdImage(models.Model):
    id = models.BigAutoField(primary_key=True)
    create_time = models.DateTimeField('创建时间', auto_now_add=True)
    ad_image = models.ImageField('广告图', upload_to='ad', null=True, blank=True)
    url = models.CharField('广告链接', max_length=256, null=True, blank=True)

    class Meta:
        ordering = ['-id']
        verbose_name = '广告图'
        verbose_name_plural = verbose_name

    def to_dict(self):
        result = {
            'id': self.id,
            'create_time': self.create_time,
            'ad_image': MEDIA_URL + str(self.ad_image),
            'href': self.url
        }
        return result


class UrlModel(models.Model):
    id = models.BigAutoField(primary_key=True)
    create_time = models.DateTimeField('创建时间', auto_now_add=True)
    url = models.CharField('直播源链接', max_length=256, null=True, blank=True)

    class Meta:
        ordering = ['-id']
        verbose_name = '直播链接'
        verbose_name_plural = verbose_name
