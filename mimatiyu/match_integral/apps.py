from django.apps import AppConfig


class MatchIntegralConfig(AppConfig):
    name = 'match_integral'
    verbose_name = "内容管理"
    def ready(self):
        import match_integral.signals
