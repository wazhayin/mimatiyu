import requests
import time
import re

from match_integral.models import LeagueScore

def run():
    headers = {
        "Host": "zq.win007.com",
        "Referer": "http://zq.win007.com/info/index_cn.htm",
        "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36"
    }
    now = time.time()
    hour = time.strftime('%Y%m%d%H', time.localtime(now))
    # 英超:36, 意甲:34, 西甲:34, 德甲:8, 法甲:11
    league_ids = [36, 34, 31, 8, 11]
    for id in league_ids:
        s = requests.Session()
        r = s.get('http://zq.win007.com/jsData/matchResult/2018-2019/s{}.js?version={}'.format(id, hour),
                  headers=headers)
        with open('./{}.txt'.format(id), 'w') as f:
            f.write(r.text)


    league_ids = [36, 34, 31, 8, 11]
    for id in league_ids:
        with open('match_integral/{}.txt'.format(id), 'r') as f:
            data = f.read()
        team_compile = r"^var arrTeam = (.*);$"
        arrTeam = re.findall(team_compile, data, re.MULTILINE)
        team_lists = list(eval(arrTeam[0]))
        team_dic = {}
        league_compile = r"^var arrLeague = (.*);$"
        arrLeague = re.findall(league_compile, data, re.MULTILINE)
        league = list(eval(arrLeague[0]))

        for team in team_lists:
            team_dic[team[0]] = team[1]

        score_compile = r"^var totalScore = (.*);$"
        arrScore = re.findall(score_compile, data, re.MULTILINE)
        scorelists = list(eval(arrScore[0]))
        for score in scorelists:
            LeagueScore.objects.create(
                league_name=league[1], ranking=score[1], club_name=team_dic[score[2]],
                win_count=score[4], draw_count=score[5], lose_count=score[6], integral=score[16]
            )

