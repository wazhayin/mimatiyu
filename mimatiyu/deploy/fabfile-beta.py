from fabric.api import cd, run, env

env.use_ssh_config = True

env.hosts = ['tiyu']

remote_temp_dir = '/root/temp/mimatiyu/'
remote_dir = '/root/wang/mimatiyu/'


def update_code():
    with cd(remote_temp_dir):
        run('git pull')
        run('cp -r mimatiyu {}'.format(remote_dir))


def reload_daphne():
    run('sudo supervisorctl -c /etc/mimatiyu/supervisord.conf restart mimatiyu_runserver')


def migrate():
    run(
        '/root/.virtualenvs/mimatiyu/bin/python '
        '/root/wang/mimatiyu/mimatiyu/manage.py '
        'migrate'
    )


def makemigrations():
    run(
        '/root/.virtualenvs/mimatiyu/bin/python '
        '/root/wang/mimatiyu/mimatiyu/manage.py '
        'makemigrations'
    )


def update():
    update_code()
    reload_daphne()


def update_migrate():
    update_code()
    makemigrations()
    migrate()
    reload_daphne()
